# DALABOOKS

O projeto está dividido em duas pastas. O projeto de java encontra-se na pasta "back" e a parte em js na pasta "front"

- Necessário instalar: 

	- gitBash (https://gitforwindows.org/)
	- intellij


- Como baixar o projeto?

	1. clicar no botao "clone" na parte direita superior da página. Escolher opção https.
	2. abrir o git bash na pasta q deseja salvar o projeto. Colocar o comando:
		git clone codigoCopiadoHttps
	3. o projeto está baixado, agora só abrir no intellij


- Como rodar o back?
	1. abrir o projeto no intellij e aguardar baixar as dependências, pode demorar bastante esse processo.
	2. abrir a classe DalaBooksApplication e clicar no play verde na lateral
	3. resultado esperado: Completed initialization in 8 ms

	Obs 1: as classes estão no path dalabooks\back\src\main\java\com\programacao\dalaBooks
	Obs 2: os endpoints estão dentro de "web"

- Como acessar os endpoints?
	1. instalar o postman   (https://www.postman.com/)
		obs: não é necessário ter uma conta pra usar. Tem alguma forma de conseguir fazer as coisas sem
	2. baixar a collection na pasta postman
	3. ir na aba "import" do postman e arrastar o arquivo
	4. abrir o request POST - INICIAR DADOS e clicar "send". Após isso já poderá listar clientes e livros etc

- Como rodar o front?
	1. necessário instalar node (https://nodejs.org/en/)
	2. abrir no gitbash a pasta front
	3. rodar o comando  **npm install** e aguardar - pode demorar
	4. rodar o comando  **npm start**
	5. aguardar até q abra uma guia no chrome no endereço localhost:3000

	Obs 1: apenas terá livros se já tiver rodado o endpoint "iniciar dados" no postman
	Obs 2: caso dê erro no npm install, tentar rodar de novo
	

- **Como é pra funcionar: **

	Eu fiz algumas operações, que estão divididas de acordo com o tema, como clientes e livros. Assim, tanto no java como no postman a estrutura é semelhante.
Para ter uma base de dados prévia, fiz o endpoint /iniciar-dados, que vai criar alguns objetos. No front, para realizar alguma operação de compra ou empréstimo, **é necessário ir na aba "clientes" e selecionar um para atender**, após isso já é possível retirar ou comprar livros.
Pelo postman, é só acessar a operação "venda" ou "emprestimo" dentro da pasta "Registro".

	Caso aconteca algum erro de codigo no web, só dar um f5.
