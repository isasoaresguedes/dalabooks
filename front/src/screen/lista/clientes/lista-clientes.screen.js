import React, { useState, useEffect } from 'react'
import { ClienteService } from '../../../service/cliente.service'
import { makeStyles } from '@material-ui/core/styles'
import { CardCliente } from '../../../component/index'
import Box from '@material-ui/core/Box';
import { LocalStorageService } from '../../../service/localStorage.service'

export const ListaClientesScreen = () => {

    const clienteService = new ClienteService()
    const localStorage = new LocalStorageService()
    const classes = useStyles();
    const [clientes, setClientes] = useState([])

    useEffect(() => {
        if (clientes.length > 0) return
        async function getLivrosDisponiveis() {
            const clientes = await clienteService.getClientes()
            // setLivros(livros)
            setClientes(clientes)
            console.log(clientes)
        }

        getLivrosDisponiveis();
    }, []);

    const atenderCliente = (cliente) => {
        localStorage.adicionarCliente(cliente)
    }

    const renderClientes = () => {
        if (clientes.length > 0) {
            return (
                clientes.map((cliente, key) => {
                    return <CardCliente key={key} cliente={cliente} onClick={() => atenderCliente(cliente)} />
                })
            )
        }
        return null
    }

    return (
        <Box className={classes.container}>
            <Box className={classes.grid}>
                {renderClientes()}
            </Box>
        </Box>
    )

}

const useStyles = makeStyles({
    grid: {
        display: "flex",
        flexWrap: "wrap",
        margin: "10px",
    },
    card: {
        margin: "5px"
    },
    container: {
        justifyContent: 'center'
    }
});