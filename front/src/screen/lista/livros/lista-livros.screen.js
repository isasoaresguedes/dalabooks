import React, { useState, useEffect } from 'react'
import Button from '@material-ui/core/Button';
import { LivroService } from '../../../service/livro.service'
import { makeStyles } from '@material-ui/core/styles';
import { CardLivro } from '../../../component/card/card-livro/card-livro.component'
import Box from '@material-ui/core/Box';
import { LocalStorageService } from '../../../service/localStorage.service'

export const ListaLivrosScreen = () => {

    const livroService = new LivroService()
    const localStorage = new LocalStorageService()
    const classes = useStyles();
    const [livros, setLivros] = useState([])

    useEffect(() => {
        if (livros.length > 0) return
        async function getLivrosDisponiveis() {
            const livros = await livroService.getLivrosDisponiveis()
            setLivros(livros)
        }
        getLivrosDisponiveis();
    }, [livros.length]);

    const listagemLivros = async () => {
        let livros = await livroService.getLivrosDisponiveis()

        setLivros(livros)
        console.log(livros)
    }

    const listagemLivrosVenda = async () => {
        let livros = await livroService.getLivrosDisponiveis()
        livros = livros.filter((livro) => {
            return livro.tipo === 'VENDA'
        })
        setLivros(livros)
        console.log(livros)
    }

    const listagemLivrosEmpretimo = async () => {
        let livros = await livroService.getLivrosDisponiveis()
        livros = livros.filter((livro) => {
            return livro.tipo === 'EMPRESTIMO'
        })
        setLivros(livros)
    }

    const adicionarLivroCarrinho = (item) => {
        localStorage.adicionarLivro('livro', item)
    }

    const renderLivros = () => {
        if (livros.length > 0) {
            return (
                livros.map((livro, key) => {
                    return <CardLivro key={key} livro={livro} class={classes.card} onClick={() => adicionarLivroCarrinho(livro)} />
                })
            )
        }
        return null
    }

    return (
        <Box className={classes.container}>
            <Button color="secondary" onClick={() => listagemLivrosEmpretimo()}>emprestimo</Button>
            <Button color="secondary" onClick={() => listagemLivrosVenda()}>venda</Button>
            <Button color="secondary" onClick={() => listagemLivros()}>todos</Button>
            <Box className={classes.grid}>
                {renderLivros()}
            </Box>
        </Box>
    )

}

const useStyles = makeStyles({
    grid: {
        display: "flex",
        flexWrap: "wrap",
        margin: "10px",
    },
    card: {
        margin: "5px"
    },
    container: {
        justifyContent: 'center'
    }
});