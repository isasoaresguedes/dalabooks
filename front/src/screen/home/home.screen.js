import React from 'react'
import { makeStyles } from '@material-ui/core/styles'
import AppBar from '@material-ui/core/AppBar'
import Tabs from '@material-ui/core/Tabs'
import Tab from '@material-ui/core/Tab'
import Typography from '@material-ui/core/Typography'
import Box from '@material-ui/core/Box'
import { ListaLivrosScreen, ListaClientesScreen, CarrinhoScreen } from '../index'
import MenuBookIcon from '@material-ui/icons/MenuBook'
import ShoppingCartIcon from '@material-ui/icons/ShoppingCart'

const TabPanel = (props) => {
    const { children, value, index, ...other } = props;

    return (
        <div
            role="tabpanel"
            hidden={value !== index}
            id={`simple-tabpanel-${index}`}
            aria-labelledby={`simple-tab-${index}`}
            {...other}
        >
            {value === index && (
                <Box p={3}>
                    {children}
                </Box>
            )}
        </div>
    )
}

function a11yProps(index) {
    return {
        id: `simple-tab-${index}`,
        'aria-controls': `simple-tabpanel-${index}`,
    };
}

export const HomeScreen = () => {

    const [value, setValue] = React.useState(0);
    const classes = useStyles();

    const handleChange = (event, newValue) => {
        setValue(newValue);
    }

    return (

        <div className={classes.root}>
            <AppBar position="static" className={classes.bar}>
                <MenuBookIcon className={classes.icon} color="secondary" />
                <Typography variant="overline" display="block" >
                    DalaBooks
                </Typography>
                <Tabs value={value} onChange={handleChange} aria-label="simple tabs example">
                    <Tab label="Livros" {...a11yProps(0)} />
                    <Tab label="Diversos" {...a11yProps(1)} />
                    <Tab label="Clientes" {...a11yProps(2)} />
                    <Tab label="Estoque" {...a11yProps(3)} />
                    <Tab icon={<ShoppingCartIcon color="secondary" onClick={() => { a11yProps(4) }} />}></Tab>
                </Tabs>
            </AppBar>
            <TabPanel value={value} index={0}>
                <ListaLivrosScreen />
            </TabPanel>
            <TabPanel value={value} index={1}>
                Diversos
            </TabPanel>
            <TabPanel value={value} index={2}>
                <ListaClientesScreen />
            </TabPanel>
            <TabPanel value={value} index={3}>
                Estoque
            </TabPanel>
            <TabPanel value={value} index={4}>
                <CarrinhoScreen />
            </TabPanel>
        </div >
    )

}

const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
        backgroundColor: theme.palette.background.paper,
    },
    bar: {
        flexDirection: 'row',
        alignItems: 'center',
        backgroundColor: 'white',
        color: 'black'
    },
    icon: {
        marginLeft: '20px',
        marginRight: '20px'
    }
}));
