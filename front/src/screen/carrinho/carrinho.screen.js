import React, { useState, useEffect } from 'react';
import Box from '@material-ui/core/Box';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';

import { ListaVendaSection } from './section/lista-venda.section'
import { ListaEmprestimoSection } from './section/lista-emprestimo.section'
import { LocalStorageService } from '../../service/localStorage.service'
import { RegistroService } from '../../service/registro.service'
import { ResumoSection } from './section/resumo-compra.section'

export const CarrinhoScreen = () => {

    const style = useStyles()
    const localStorage = new LocalStorageService()
    const registroService = new RegistroService()
    const [exibirCompra, setExibirCompra] = useState(false)
    const [response, setResponse] = useState({})
    const [valorTotal, setValorTotal] = useState(0)

    useEffect(() => {
        // const mock = {
        //     resultEmprestimo: {
        //         livrosEmprestados: [
        //             { codigo: 0, quantidade: 1, nome: "Harry Potter e a Pedra Filosofal", preco: 0 },
        //             { codigo: 2, quantidade: 0, nome: "Harry Potter e o Cálice de Fogo", preco: 0 }
        //         ],
        //         valorTotal: 5
        //     },
        //     resultVenda: {
        //         produtosProcessados: [
        //             { codigo: 8, quantidade: 2, nome: "Anne de Green Gables", preco: 28 },
        //             { codigo: 10, quantidade: 1, nome: "1984", preco: 29.9 },
        //             { codigo: 11, quantidade: 1, nome: "Ponto de Impacto", preco: 32 }
        //         ],
        //         valorTotal: 0
        //     }
        // }
        setValorTotal(registroService.retornarValorTotal())
        // setResponse(mock)
        // setExibirCompra(true)
    }, []);

    const comprar = async () => {
        const listaProdutos = localStorage.getProdutosStorage()
        const listaEmprestimo = localStorage.getLivrosEmprestimoStorage()
        let response2 = await registroService.comprar(listaProdutos, listaEmprestimo)
        console.log(response2)
        setResponse(response2)
        setExibirCompra(true)
    }

    const exibirCardCompra = () => {
        let venda = localStorage.getObject('livro.venda') || [{}]
        let emprestimo = localStorage.getObject('livro.emprestimo') || [{}]
        const exibir = venda.length > 0 || emprestimo.length > 0
        if (exibir) {
            return renderCardResumo
        }
        return (
            <div>Ops... Carrinho está vazio!</div>
        )
    }

    const renderCardResumo = (
        <Card className={style.root}>
            <CardContent>
                <Typography className={style.title} color="textSecondary" gutterBottom>
                    Valor total
                </Typography>
                <Typography variant="h5" component="h2">
                    {valorTotal.toFixed(2)}
                </Typography>
            </CardContent>
            <CardActions>
                <Button size="small" color="secondary" variant="contained" onClick={() => comprar()}>Comprar</Button>
            </CardActions>
        </Card>
    )

    const renderCarrinho = (
        <Box className={style.container}>
            <Box className={style.table}>
                <ListaVendaSection />
            </Box>
            <ListaEmprestimoSection />
            {exibirCardCompra()}
        </Box>
    )

    const renderResumoCompra = (
        <ResumoSection resumo={response} />
    )

    return (
        exibirCompra ? renderResumoCompra : renderCarrinho
    )
}

const useStyles = makeStyles({
    container: {
        height: '100%',
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'flex-end',
        alignItems: 'flex-end'
    },
    table: {
        marginBottom: '10px',
        width: '100%'
    },
    root: {
        marginTop: '10px',
        width: 200,
    },
    bullet: {
        display: 'inline-block',
        margin: '0 2px',
        transform: 'scale(0.8)',
    },
    title: {
        fontSize: 14,
    },
    pos: {
        marginBottom: 12,
    },
});
