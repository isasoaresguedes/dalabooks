import React from 'react';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';

export const ResumoSection = (props) => {
    const classes = useStyles()

    const infoLivros = (produto, valor) => {
        return (
            <>
                <Box className={classes.produto}>
                    <Box className={classes.margin}>
                        <Typography color="textSecondary">
                            Produto
                    </Typography>
                        <Typography className={classes.pos} variant="body2" component="p">
                            {produto.nome}
                        </Typography>
                    </Box>

                    <Box>
                        <Typography color="textSecondary">
                            Quantidade
                    </Typography>
                        <Typography className={classes.pos} variant="body2" component="p">
                            {valor}
                        </Typography>
                    </Box>
                </Box>
            </>
        )
    }

    const renderVenda = () => {
        if (!!props.resumo.resultVenda.produtosProcessados && props.resumo.resultVenda.produtosProcessados.length > 0) {
            return (
                <Box className={classes.separacao}>
                    <Card className={classes.root} variant="outlined">
                        <CardContent>
                            <Typography variant="h5" component="h2">
                                Produtos Vendidos
                            </Typography>
                            <Typography variant="body1" gutterBottom>Livros retirados:</Typography>
                            {props.resumo.resultVenda.produtosProcessados.map((livro, key) => {
                                let valor = props.resumo.resultVenda.quantidadesProcessadas[key]
                                return infoLivros(livro, valor)
                            })}
                            <Typography variant="body1" gutterBottom>Valor total: {(props.resumo.resultVenda.valorTotal).toFixed(2)}</Typography>
                        </CardContent>
                    </Card>
                </Box>
            )
        }
        return null
    }

    const renderEmprestimo = () => {
        if (!!props.resumo.resultEmprestimo.livrosEmprestados && props.resumo.resultEmprestimo.livrosEmprestados.length > 0) {
            return (
                <Box className={classes.separacao}>
                    <Card className={classes.root} variant="outlined">
                        <CardContent>
                            <Typography variant="h5" component="h2">
                                Produtos Retirados
                            </Typography>
                            <Typography variant="body1" gutterBottom>Livros retirados:</Typography>
                            {props.resumo.resultEmprestimo.livrosEmprestados.map((livro, key) => {
                                let valor = props.resumo.resultEmprestimo.quantidadesAprovadas[key]
                                return infoLivros(livro, valor)
                            })}
                            <Typography variant="body1" gutterBottom>Valor total: {(props.resumo.resultEmprestimo.valorTotal).toFixed(2)}</Typography>
                            <Typography variant="body1" color="secondary" gutterBottom>{props.resumo.resultEmprestimo.dataRetornar}</Typography>
                        </CardContent>
                    </Card>
                </Box>
            )
        }
    }

    return (
        <>
            <Typography variant="h6" gutterBottom>
                RESUMO DA COMPRA
            </Typography>
            {renderVenda(props)}
            {renderEmprestimo(props)}
        </>
    )
}

const useStyles = makeStyles({
    valorTotal: {
        marginTop: '10px',
    },
    root: {
        minWidth: 275,
    },
    bullet: {
        display: 'inline-block',
        margin: '0 2px',
        transform: 'scale(0.8)',
    },
    title: {
        fontSize: 14,
    },
    pos: {
        marginBottom: 12,
    },
    produto: {
        display: 'flex',
        flexDirection: 'row',
        width: '400px'
    },
    margin: {
        marginRight: '20px',
        width: '400px'
    },
    separacao: {
        marginTop: '20px'
    }
});