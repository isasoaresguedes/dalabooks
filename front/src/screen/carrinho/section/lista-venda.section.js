import React, { useState, useEffect } from 'react';

import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import { LocalStorageService } from '../../../service/localStorage.service'

export const ListaVendaSection = (props) => {
    const classes = useStyles()
    const storageService = new LocalStorageService()
    const [listaProdutos, setProdutos] = useState([])

    useEffect(() => {
        const listaProdutos = storageService.getProdutosStorage()
        setProdutos(listaProdutos)
    }, [])

    const renderListaProdutos = () => {
        if (listaProdutos.length > 0) {
            return (
                <TableContainer component={Paper}>
                    <Table className={props.class ? `${classes.table} ${props.class}` : classes.table} aria-label="simple table">
                        <TableHead className={classes.head}>
                            <TableRow>
                                <TableCell>Produtos para comprar :)</TableCell>
                                <TableCell align="right">Autor</TableCell>
                                <TableCell align="right">Quantidade</TableCell>
                                <TableCell align="right">Valor</TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {listaProdutos.map((row) => (
                                <TableRow key={row.nome}>
                                    <TableCell component="th" scope="row">
                                        {row.nome}
                                    </TableCell>
                                    <TableCell align="right">{row.autor}</TableCell>
                                    <TableCell align="right">{row.quantidadeCarrinho}</TableCell>
                                    <TableCell align="right">{(row.preco * row.quantidadeCarrinho).toFixed(2)}</TableCell>
                                </TableRow>
                            ))}
                        </TableBody>
                    </Table>
                </TableContainer>
            )
        }
        return <></>
    }

    return (
        renderListaProdutos()
    )
}

const useStyles = makeStyles({
    table: {
        minWidth: 650,
    },
    head: {
        backgroundColor: '#d19eaa'
    }
});