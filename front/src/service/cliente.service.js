import { BaseService } from './base.service'

export class ClienteService extends BaseService {
    constructor() {
        super('http://localhost:8100/dala-books/')
    }

    async getClientes() {
        const result = await super.get('cliente')

        return result;
    }
}
