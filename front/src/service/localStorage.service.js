export class LocalStorageService {

    constructor() {
        this.keyEmprestimo = "livro.emprestimo"
        this.keyVenda = "livro.venda"
        this.keyDiversos = 'diverso.venda'
        this.keyClientes = 'cliente'
    }

    setString(key, value) {
        localStorage.setItem(key, value)
    }

    setObject(key, object) {
        const objString = JSON.stringify(object)
        localStorage.setItem(key, objString)
    }

    getString(key) {
        return localStorage.getItem(key)
    }

    getObject(key) {
        const json = localStorage.getItem(key)
        return JSON.parse(json)
    }

    removeString(key) {
        localStorage.removeItem(key)
    }

    adicionarLivro(key, livro) {

        livro.quantidadeCarrinho = 1
        if (livro.tipo === 'VENDA') {
            let lista = this.getObject(this.keyVenda) || []
            let diferente = true
            lista.forEach(element => {
                if (element.codigo === livro.codigo) {
                    diferente = false
                    element.quantidadeCarrinho = element.quantidadeCarrinho + 1
                }
            })
            if (diferente) {
                lista.push(livro)
            }
            this.setObject(this.keyVenda, lista)
        } else {
            let lista = this.getObject(this.keyEmprestimo) || []
            let diferente = true
            lista.forEach(element => {
                if (element.codigo === livro.codigo) {
                    diferente = false
                    element.quantidadeCarrinho = element.quantidadeCarrinho + 1
                }
            })
            if (diferente) {
                lista.push(livro)
            }
            this.setObject(this.keyEmprestimo, lista)
        }
    }

    getProdutosStorage() {
        return this.getObject(this.keyVenda)
    }

    getLivrosEmprestimoStorage() {
        return this.getObject(this.keyEmprestimo)
    }

    limparStorage() {
        this.setObject(this.keyVenda, [])
        this.setObject(this.keyEmprestimo, [])
        this.setObject(this.keyDiversos, [])
    }

    adicionarCliente(cliente) {
        this.setObject(this.keyClientes, cliente)
    }

}