import { BaseService } from './base.service'

export class LivroService extends BaseService {
    constructor() {
        super('http://localhost:8100/dala-books/livro')
    }

    async getLivros() {
        const result = await super.get('/listar')

        return result;
    }

    async getLivrosDisponiveis() {
        const result = await super.get('/listar-disponiveis')

        return result
    }
}