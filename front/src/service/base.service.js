import axios from 'axios'

const httpClient = (baseURL) => axios.create({
    timeout: 30000,
    baseURL
})

export class BaseService {
    constructor(baseURL) {
        this.client = httpClient(baseURL)
    }

    async get(url) {
        const result = await this.client.get(url)

        return result.data
    }

    async post(url, data) {
        const result = await this.client.post(url, data)

        return result.data
    }

    async put(url, data) {
        const result = await this.client.put(url, data)

        return result.data
    }

    async delete(url) {
        const result = await this.client.delete(url)

        return result.data
    }
}
