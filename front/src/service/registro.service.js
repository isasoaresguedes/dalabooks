import { BaseService } from './base.service'
import swal from 'sweetalert';
import { LocalStorageService } from './localStorage.service'

export class RegistroService extends BaseService {
    constructor() {
        super('http://localhost:8100/dala-books/registro')
        this.LocalStorageService = new LocalStorageService()
    }

    async comprar(listaProdutos, listaEmprestimo) {
        const codigoCliente = this.LocalStorageService.getObject('cliente').codigo
        let resultVenda = {}
        let resultEmprestimo = {}
        if (listaProdutos.length > 0) {
            const produtos = listaProdutos.map((item) => {
                return item.codigo
            })
            const quantidades = listaProdutos.map((item) => {
                return item.quantidadeCarrinho
            })
            const request = {
                codigoCliente,
                quantidades,
                produtos
            }
            resultVenda = await super.post('/venda', request)
            console.log(resultVenda)
        }
        if (listaEmprestimo.length > 0) {
            const codigos = listaEmprestimo.map((item) => {
                return item.codigo
            })
            const quantidades = listaEmprestimo.map((item) => {
                return item.quantidadeCarrinho
            })
            const request = {
                codigoCliente,
                codigos,
                quantidades
            }
            resultEmprestimo = await super.post('/emprestimo', request)
            console.log(resultEmprestimo)
        }
        swal("Feito!", "Compra concluída!", "success");
        this.LocalStorageService.limparStorage()

        return { resultVenda, resultEmprestimo };
    }

    retornarValorTotal() {
        let array = this.LocalStorageService.getObject('livro.venda')
        let soma = 0
        array.forEach(element => {
            soma += element.quantidadeCarrinho * element.preco
        });
        console.log(soma)
        return soma
    }

}