import React from 'react'
import Button from '@material-ui/core/Button'
import Card from '@material-ui/core/Card'
import CardActions from '@material-ui/core/CardActions'
import CardContent from '@material-ui/core/CardContent'
import Typography from '@material-ui/core/Typography'
import { makeStyles } from '@material-ui/core/styles'
import CardMedia from '@material-ui/core/CardMedia'

export const CardCliente = (props) => {

    const classes = useStyles();

    return (
        <Card className={props.class ? `${classes.card} ${props.class}` : classes.card}>
            <CardContent>
                <CardMedia
                    className={classes.media}
                    image={props.cliente.fotoPerfil || "https://cdn.pixabay.com/photo/2015/10/05/22/37/blank-profile-picture-973460_960_720.png"}
                />
                <Typography variant="h6" component="h2">
                    {props.cliente.nome}
                </Typography>
                <div className={classes.linha}>
                    <Typography color="textSecondary">
                        {"Pacote:"}
                    </Typography>
                    <Typography color="textSecondary">
                        {props.cliente.pacote || ""}
                    </Typography>
                </div>
                <Typography variant="body2" component="p">
                    {props.cliente.leitor ? "Leitor" : ""}
                </Typography>
            </CardContent>
            <CardActions>
                <Button size="small" onClick={props.onClick}>
                    {"Atender"}
                </Button>
            </CardActions>
        </Card>
    )
}

const useStyles = makeStyles({
    card: {
        width: 275,
    },
    bullet: {
        display: 'inline-block',
        margin: '0 2px',
        transform: 'scale(0.8)',
    },
    title: {
        fontSize: 14,
    },
    pos: {
        marginBottom: 12,
    },
    media: {
        maxWidth: '245px',
        height: '380px'
    },
    linha: {
        display: 'flex'
    }
});