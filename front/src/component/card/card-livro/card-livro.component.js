import React from 'react'
import Button from '@material-ui/core/Button'
import Card from '@material-ui/core/Card'
import CardActions from '@material-ui/core/CardActions'
import CardContent from '@material-ui/core/CardContent'
import Typography from '@material-ui/core/Typography'
import { makeStyles } from '@material-ui/core/styles'
import CardMedia from '@material-ui/core/CardMedia'

export const CardLivro = (props) => {

    const classes = useStyles();

    return (
        <Card className={props.class ? `${classes.card} ${props.class}` : classes.card}>
            <CardContent>
                <CardMedia
                    className={classes.media}
                    image={props.livro.imagem || "https://lojasaraiva.vteximg.com.br/arquivos/ids/12100486/1007942370.jpg?v=637142216975430000"}
                />
                <Typography variant="h6" component="h2">
                    {props.livro.nome}
                </Typography>
                <Typography color="textSecondary">
                    {props.livro.colecao || ""}
                </Typography>
                <Typography variant="body2" component="p">
                    {props.livro.autor}
                </Typography>
                {
                    props.livro.tipo === 'VENDA'
                        ? <Typography variant="h6" component="h2">
                            R$ {props.livro.preco.toFixed(2)}
                        </Typography>
                        : null
                }
            </CardContent>
            <CardActions>
                <Button size="small" onClick={props.onClick}>
                    {props.livro.tipo === 'VENDA' ? 'Comprar' : 'Retirar'}
                </Button>
            </CardActions>
        </Card>
    )
}

const useStyles = makeStyles({
    card: {
        width: 275,
    },
    bullet: {
        display: 'inline-block',
        margin: '0 2px',
        transform: 'scale(0.8)',
    },
    title: {
        fontSize: 14,
    },
    pos: {
        marginBottom: 12,
    },
    media: {
        maxWidth: '245px',
        height: '380px'
    },
});