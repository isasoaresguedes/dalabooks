import React, { Fragment } from 'react';
import './App.css';
import { Route } from 'react-router-dom'
import { HomeScreen, ListaLivrosScreen } from './screen/index'

function App() {

  return (
    <Fragment>
      <Route path="/" component={HomeScreen} exact />
      <Route path="/lista/livros" component={ListaLivrosScreen} exact />
    </Fragment>

  )
}

export default App;
