package com.programacao.dalaBooks.web.response;

import com.programacao.dalaBooks.domain.Livro;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

public class EmprestimoResponse {

    List<Livro> livrosEmprestados;
    BigDecimal valorTotal;
    List<Integer> quantidadesAprovadas;
    String dataRetornar;

    public void setDataRetornar(String dataRetornar) {
        this.dataRetornar = dataRetornar;
    }

    public void setQuantidadesAprovadas(List<Integer> quantidadesAprovadas) {
        this.quantidadesAprovadas = quantidadesAprovadas;
    }

    public void setLivrosEmprestados(List<Livro> livrosEmprestados) {
        this.livrosEmprestados = livrosEmprestados;
    }

    public void setValorTotal(BigDecimal valorTotal) {
        this.valorTotal = valorTotal;
    }

    public List<Livro> getLivrosEmprestados() {
        return livrosEmprestados;
    }

    public BigDecimal getValorTotal() {
        return valorTotal;
    }

    public List<Integer> getQuantidadesAprovadas() {
        return quantidadesAprovadas;
    }

    public String getDataRetornar() {
        return dataRetornar;
    }

}
