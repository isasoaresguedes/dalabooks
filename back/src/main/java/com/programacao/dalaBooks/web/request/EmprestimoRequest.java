package com.programacao.dalaBooks.web.request;

import java.util.List;

public class EmprestimoRequest {

    List<Integer> codigos;
    Integer codigoCliente;
    List<Integer> quantidades;

    public Integer getCodigoCliente() {
        return codigoCliente;
    }

    public List<Integer> getCodigos() {
        return codigos;
    }

    public List<Integer> getQuantidades() {
        return  quantidades;
    }
}
