package com.programacao.dalaBooks.web;

import com.programacao.dalaBooks.domain.Livro;
import com.programacao.dalaBooks.repository.LivroRepository;
import com.programacao.dalaBooks.service.LivroService;
import com.programacao.dalaBooks.web.request.LivroRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin("*")
@RestController
@RequestMapping("/livro")
public class LivroController {

    @Autowired
    LivroRepository livroRepository;

    @Autowired
    LivroService livroService;

    @PostMapping("/cadastrar")
    public Livro cadastrar(@RequestBody LivroRequest request) {
        return livroService.cadastrar(request);
    }

    @GetMapping("/listar")
    public List<Livro> listar() {
        return livroRepository.listarTodos();
    }

    @GetMapping("/listar-indisponiveis")
    public List<Livro> listarIndisponiveis() {
        return livroRepository.findIndisponiveis();
    }

    @GetMapping("/listar-disponiveis")
    public List<Livro> listarDisponiveis() {
        return livroRepository.findDisponiveis();
    }

    @PutMapping("/atualizar/{codigo}")
    public Livro atualizar(@RequestBody LivroRequest request, @PathVariable("codigo") int codigo) {
        return livroService.atualizar(request, codigo);
    }

}
