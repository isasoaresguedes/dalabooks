package com.programacao.dalaBooks.web;

import com.programacao.dalaBooks.domain.Cliente;
import com.programacao.dalaBooks.repository.ClienteRepository;
import com.programacao.dalaBooks.service.ClienteService;
import com.programacao.dalaBooks.web.request.ClienteRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin("*")
@RestController
@RequestMapping("/cliente")
public class ClienteController {

    @Autowired
    ClienteService clienteService;

    @Autowired
    ClienteRepository clienteRepository;

    @PostMapping("/cadastrar")
    public Cliente cadastrar( @RequestBody ClienteRequest request) {
        return clienteService.cadastrar(request);
    }

    @GetMapping()
    public List<Cliente> listar() {
        return clienteRepository.listarTodos();
    }

    @PutMapping("/atualizar")
    public Cliente atualizar(@RequestBody ClienteRequest request, @PathVariable int codigo) {
        return clienteService.atualizar(request, codigo);
    }
}
