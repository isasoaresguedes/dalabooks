package com.programacao.dalaBooks.web.response;

import com.programacao.dalaBooks.domain.Produto;

import java.math.BigDecimal;
import java.util.List;

public class VendaResponse {

    BigDecimal valorTotal;
    List<Produto> produtosProcessados;
    List<Integer> quantidadesProcessadas;

    public void setValorTotal(BigDecimal valorTotal) {
        this.valorTotal = valorTotal;
    }

    public void setProdutosProcessados(List<Produto> produtosProcessados) {
        this.produtosProcessados = produtosProcessados;
    }

    public void setQuantidadesProcessadas(List<Integer> quantidadesProcessadas) {
        this.quantidadesProcessadas = quantidadesProcessadas;
    }

    public BigDecimal getValorTotal() {
        return valorTotal;
    }

    public List<Produto> getProdutosProcessados() {
        return produtosProcessados;
    }

    public List<Integer> getQuantidadesProcessadas() {
        return  quantidadesProcessadas;
    }
}
