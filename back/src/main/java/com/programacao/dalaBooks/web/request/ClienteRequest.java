package com.programacao.dalaBooks.web.request;
import com.programacao.dalaBooks.domain.Pacote;

public class ClienteRequest {
    String nome;
    Boolean leitor;
    Pacote pacote;
    String foto;

    public String getFoto() {
        return foto;
    }

    public String getNome() {
        return nome;
    }

    public Boolean isLeitor() {
        return leitor;
    }

    public Pacote getPacote() {
        return pacote;
    }
}
