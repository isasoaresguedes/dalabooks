package com.programacao.dalaBooks.web;

import com.programacao.dalaBooks.domain.Diverso;
import com.programacao.dalaBooks.domain.Produto;
import com.programacao.dalaBooks.repository.DiversoRepository;
import com.programacao.dalaBooks.service.DiversoService;
import com.programacao.dalaBooks.web.request.DiversoRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin("*")
@RestController
@RequestMapping("/diverso")
public class DiversoController {

    @Autowired
    DiversoService diversoService;

    @Autowired
    DiversoRepository diversoRepository;

    @PostMapping("/cadastrar")
    public Produto cadastrar(@RequestBody DiversoRequest request) {
        return diversoService.cadastrar(request);
    }

    @GetMapping("/listar")
    public List<Diverso> listar() {
        return diversoRepository.listarTodos();
    }

    @GetMapping("/listar-indisponiveis")
    public List<Diverso> listarIndisponiveis() {
        return diversoRepository.findIndisponiveis();
    }

    @GetMapping("/listar-disponiveis")
    public List<Diverso> listarDosponiveis() {
        return diversoRepository.findDisponiveis();
    }

    @PutMapping("/atualizar/{codigo}")
    public Diverso atualizar(@RequestBody DiversoRequest request, @PathVariable("codigo") int codigo) {
        return diversoService.atualizar(request, codigo);
    }
}
