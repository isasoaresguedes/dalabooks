package com.programacao.dalaBooks.web.request;

import java.util.List;

public class VendaRequest {

    List<Integer> produtos;
    List<Integer> quantidades;
    int codigoCliente;

    public List<Integer> getProdutos() {
        return produtos;
    }

    public int getCodigoCliente() {
        return codigoCliente;
    }

    public List<Integer> getQuantidades() {
        return  quantidades;
    }
}
