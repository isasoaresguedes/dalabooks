package com.programacao.dalaBooks.web.request;

import com.programacao.dalaBooks.domain.Genero;
import com.programacao.dalaBooks.domain.Tipo;

import java.util.List;

public class LivroRequest {

    int quantidade;
    String nome;
    String preco;
    String autor;
    String editora;
    String colecao;
    List<Genero> generos;
    Tipo tipo;
    String imagem;

    public String getImagem() {
        return imagem;
    }

    public int getQuantidade() {
        return quantidade;
    }

    public String getNome() {
        return nome;
    }

    public String getPreco() {
        return preco;
    }

    public String getAutor() {
        return autor;
    }

    public String getEditora() {
        return editora;
    }

    public String getColecao() {
        return colecao;
    }

    public List<Genero> getGeneros() {
        return generos;
    }

    public Tipo getTipo() {
        return tipo;
    }
}
