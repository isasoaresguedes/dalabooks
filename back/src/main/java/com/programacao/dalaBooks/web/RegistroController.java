package com.programacao.dalaBooks.web;

import com.programacao.dalaBooks.domain.Registro;
import com.programacao.dalaBooks.repository.RegistroRepository;
import com.programacao.dalaBooks.service.EmprestimoService;
import com.programacao.dalaBooks.service.VendaService;
import com.programacao.dalaBooks.web.request.EmprestimoRequest;
import com.programacao.dalaBooks.web.request.VendaRequest;
import com.programacao.dalaBooks.web.response.EmprestimoResponse;
import com.programacao.dalaBooks.web.response.VendaResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin("*")
@RestController
@RequestMapping("/registro")
public class RegistroController {

    @Autowired
    VendaService vendaService;

    @Autowired
    EmprestimoService emprestimoService;

    @Autowired
    RegistroRepository registroRepository;

    @PostMapping("/venda")
    public VendaResponse venda(@RequestBody VendaRequest request) {
        return vendaService.vender(request);
    }

    @PostMapping("/emprestimo")
    public EmprestimoResponse emprestimo(@RequestBody EmprestimoRequest request) {
        return emprestimoService.emprestar(request);
    }

    @GetMapping
    public List<Registro> listar() {
        return registroRepository.listarTodos();
    }
}
