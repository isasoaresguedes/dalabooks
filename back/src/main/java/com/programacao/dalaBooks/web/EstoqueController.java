package com.programacao.dalaBooks.web;

import com.programacao.dalaBooks.domain.*;
import com.programacao.dalaBooks.repository.ClienteRepository;
import com.programacao.dalaBooks.repository.DiversoRepository;
import com.programacao.dalaBooks.repository.LivroRepository;
import com.programacao.dalaBooks.service.ProdutoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.List;

@CrossOrigin("*")
@RestController
@RequestMapping("/estoque")
public class EstoqueController {

    @Autowired
    ClienteRepository clienteRepository;

    @Autowired
    LivroRepository livroRepository;

    @Autowired
    ProdutoService produtoService;

    @Autowired
    DiversoRepository diversoRepository;

    @PostMapping("/solicitar/{codigo}")
    public Produto solicitarProduto(@PathVariable("codigo") int codigo, @RequestParam int quantidade) {
        return produtoService.solicitar(codigo, quantidade);
    }

    @GetMapping("/listar-todos")
    public List<Produto> listarTodosProdutos() {
        return produtoService.listarTodos();
    }

    @GetMapping("/listar-disponiveis")
    public List<Produto> listarProdutosDisponiveis() {
        return produtoService.listarDisponiveis();
    }

    @GetMapping("/listar-indisponiveis")
    public List<Produto> listarProdutosIndisponiveis() {
        return produtoService.listarIndisponiveis();
    }

    @PostMapping("/iniciar-dados")
    public void iniciar() {
        this.incluirClientes();
        this.incluirLivrosEmprestimo();
        this.incluirLivrosVenda();
        this.incluirProdutosCafe();
    }

    private void incluirClientes() {
        Cliente cliente1 = new Cliente("Emma Watson", true, Pacote.MENSAL);
        Cliente cliente2 = new Cliente("c2", true, Pacote.UNITARIO);
        Cliente cliente3 = new Cliente("c3", true, Pacote.NENHUM);
        Cliente cliente4 = new Cliente("c4", false, Pacote.NENHUM);
        Cliente cliente5 = new Cliente("c5", false, Pacote.UNITARIO);
        Cliente cliente6 = new Cliente("c6", false, Pacote.MENSAL);

        cliente1.setFotoPerfil("https://ogimg.infoglobo.com.br/in/18845220-c4a-d1d/FT1086A/652/201603081618284004_AP.jpg");
        clienteRepository.salvar(cliente1);
        clienteRepository.salvar(cliente2);
        clienteRepository.salvar(cliente3);
        clienteRepository.salvar(cliente4);
        clienteRepository.salvar(cliente5);
        clienteRepository.salvar(cliente6);
    }

    private void incluirLivrosEmprestimo() {
        Livro livro1 = new Livro(2, "Harry Potter e a Pedra Filosofal", Tipo.EMPRESTIMO);
        Livro livro2 = new Livro(1, "Harry Potter e a Câmara Secreta", Tipo.EMPRESTIMO);
        Livro livro3 = new Livro(1, "Harry Potter e o Cálice de Fogo", Tipo.EMPRESTIMO);
        Livro livro4 = new Livro(3, "Harry Potter e o Prisioneiro de Azkaban", Tipo.EMPRESTIMO);
        Livro livro5 = new Livro(2, "Harry Potter e a Ordem da Fênix", Tipo.EMPRESTIMO);
        Livro livro6 = new Livro(0, "Harry Potter e o Enigma do Príncipe", Tipo.EMPRESTIMO);
        Livro livro7 = new Livro(2, "Harry Potter e as Relíquias da Morte", Tipo.EMPRESTIMO);

        livro1.setImagem("https://lojasaraiva.vteximg.com.br/arquivos/ids/12100486/1007942370.jpg?v=637142216975430000");
        livro2.setImagem("https://lojasaraiva.vteximg.com.br/arquivos/ids/12100503/1007817940.jpg?v=637142217019530000");
        livro3.setImagem("https://images-na.ssl-images-amazon.com/images/I/8172dLr8Z7L.jpg");
        livro4.setImagem("https://pictures.abebooks.com/isbn/9788532512062-uk.jpg");
        livro5.setImagem("https://http2.mlstatic.com/livro-harry-potter-e-a-ordem-da-fenix-capa-brilhante-D_NQ_NP_627401-MLB20325597286_062015-F.jpg");
        livro6.setImagem("https://i.pinimg.com/originals/0f/7a/14/0f7a140bbe592c636aaf6b7de9332470.jpg");
        livro7.setImagem("https://images-na.ssl-images-amazon.com/images/I/81PHloIwKnL.jpg");

        livro1.setAutor("J. K. Rowling");
        livro2.setAutor("J. K. Rowling");
        livro3.setAutor("J. K. Rowling");
        livro4.setAutor("J. K. Rowling");
        livro5.setAutor("J. K. Rowling");
        livro6.setAutor("J. K. Rowling");
        livro7.setAutor("J. K. Rowling");

        livro1.setColecao("Harry Potter");
        livro2.setColecao("Harry Potter");
        livro3.setColecao("Harry Potter");
        livro4.setColecao("Harry Potter");
        livro5.setColecao("Harry Potter");
        livro6.setColecao("Harry Potter");
        livro7.setColecao("Harry Potter");

        livroRepository.salvar(livro1, produtoService.gerarCodigo());
        livroRepository.salvar(livro2, produtoService.gerarCodigo());
        livroRepository.salvar(livro3, produtoService.gerarCodigo());
        livroRepository.salvar(livro4, produtoService.gerarCodigo());
        livroRepository.salvar(livro5, produtoService.gerarCodigo());
        livroRepository.salvar(livro6, produtoService.gerarCodigo());
        livroRepository.salvar(livro7, produtoService.gerarCodigo());
    }

    private void incluirLivrosVenda() {
        Livro livro1 = new Livro(4, "O Hobbit", Tipo.VENDA);
        livro1.setPreco(new BigDecimal("25.00"));
        livro1.setAutor("J. R. R. Tolkien");
        livro1.setImagem("https://elshaddai.com.br/wp-content/uploads/2019/06/O-Hobbit-J.R.R-Tolkien.jpg");

        Livro livro2 = new Livro(3, "Anne de Green Gables", Tipo.VENDA);
        livro2.setPreco(new BigDecimal("28.00"));
        livro2.setAutor("Lucy Maud Montgomery");
        livro2.setImagem("https://images-na.ssl-images-amazon.com/images/I/91r32tjyFBL.jpg");

        Livro livro3 = new Livro(2, "A Seleção", Tipo.VENDA);
        livro3.setPreco(new BigDecimal("20.00"));
        livro3.setAutor("Kiera Cass");
        livro3.setImagem("https://1.bp.blogspot.com/-3icFjGiyHVE/UJMAm8vBuFI/AAAAAAAASiI/ogfu6-_NFPI/s1600/selecao_bx.jpg");

        Livro livro4 = new Livro(2, "1984", Tipo.VENDA);
        livro4.setPreco(new BigDecimal("29.90"));
        livro4.setImagem("https://m.media-amazon.com/images/I/41E9Z5XaHcL.jpg");
        livro4.setAutor("George Orwell");

        Livro livro5 = new Livro(2, "Ponto de Impacto", Tipo.VENDA);
        livro5.setPreco(new BigDecimal("32.00"));
        livro5.setImagem("https://cache.skoob.com.br/local/images//se6rlcr2rzL5hZveFhAcoyJo-PU=/300x0/center/top/filters:format(jpeg)/https://skoob.s3.amazonaws.com/livros/298/PONTO_DE_IMPACTO_1228487893B.jpg");
        livro5.setAutor("Dan Brown");

        Livro livro6 = new Livro(3, "A Guerra dos Tronos", Tipo.VENDA);
        livro6.setPreco(new BigDecimal("55.00"));
        livro6.setImagem("https://images-na.ssl-images-amazon.com/images/I/91ytruCTL8L.jpg");
        livro6.setColecao("As Crônicas de Gelo e Fogo");
        livro6.setAutor("George R. R. Martin");

        Livro livro7 = new Livro(2, "A Fúria dos Reis", Tipo.VENDA);
        livro7.setPreco(new BigDecimal("50.00"));
        livro7.setImagem("https://images-na.ssl-images-amazon.com/images/I/61dkKEsyDQL.jpg");
        livro7.setColecao("As Crônicas de Gelo e Fogo");
        livro7.setAutor("George R. R. Martin");

        Livro livro8 = new Livro(1, "A Tormenta de Espadas", Tipo.VENDA);
        livro8.setPreco(new BigDecimal("60.00"));
        livro8.setImagem("https://lojasaraiva.vteximg.com.br/arquivos/ids/3247998-287-426/1000372893.jpg?v=637034217481830000");
        livro8.setAutor("George R. R. Martin");
        livro8.setColecao("As Crônicas de Gelo e Fogo");

        Livro livro9 = new Livro(0, "O Festim dos Corvos", Tipo.VENDA);
        livro9.setAutor("George R. R. Martin");
        livro9.setPreco(new BigDecimal("50.00"));
        livro9.setImagem("https://lojasaraiva.vteximg.com.br/arquivos/ids/188926/1000442024.jpg?v=636965945030970000");
        livro9.setColecao("As Crônicas de Gelo e Fogo");

        Livro livro10 = new Livro(0, "A Dança dos Dragões", Tipo.VENDA);
        livro10.setAutor("George R. R. Martin");
        livro10.setPreco(new BigDecimal("56.90"));
        livro10.setImagem("https://images-na.ssl-images-amazon.com/images/I/A1q+wZFZbGL.jpg");
        livro10.setColecao("As Crônicas de Gelo e Fogo");

        Livro livro11 = new Livro(4, "Herdeiros de Atlântida", Tipo.VENDA);
        livro11.setGenero(Genero.ANJOS);
        livro11.setGenero(Genero.LITERATURA_FANTASTICA);
        livro11.setColecao("Filhos do Éden");
        livro11.setPreco(new BigDecimal("32.90"));
        livro11.setEditora("Verus");
        livro11.setAutor("Eduardo Spohr");
        livro11.setImagem("https://lojasaraiva.vteximg.com.br/arquivos/ids/9199348/1008850890.jpg?v=637103744130230000");

        livroRepository.salvar(livro1, produtoService.gerarCodigo());
        livroRepository.salvar(livro2, produtoService.gerarCodigo());
        livroRepository.salvar(livro3, produtoService.gerarCodigo());
        livroRepository.salvar(livro4, produtoService.gerarCodigo());
        livroRepository.salvar(livro5, produtoService.gerarCodigo());
        livroRepository.salvar(livro6, produtoService.gerarCodigo());
        livroRepository.salvar(livro7, produtoService.gerarCodigo());
        livroRepository.salvar(livro8, produtoService.gerarCodigo());
        livroRepository.salvar(livro9, produtoService.gerarCodigo());
        livroRepository.salvar(livro10, produtoService.gerarCodigo());
        livroRepository.salvar(livro11, produtoService.gerarCodigo());
    }

    private void incluirProdutosCafe() {
        Diverso produto1 = new Diverso(5, "Café preto");
        produto1.setPreco(new BigDecimal("3.00"));
        produto1.setImagem("https://www.hypeness.com.br/wp-content/uploads/2015/07/PretoCafe-SP-9.jpg");

        Diverso produto2 = new Diverso(5, "Capuccino");
        produto2.setPreco(new BigDecimal("4.00"));
        produto2.setImagem("https://www.anamariabrogui.com.br/assets/uploads/receitas/fotos/usuario-3235-18a45e40b548a7ecbb5448c6c891a55f.jpg");

        Diverso produto3 = new Diverso(5, "Mocaccino");
        produto3.setPreco(new BigDecimal("4.00"));
        produto3.setImagem("https://lh3.googleusercontent.com/proxy/GYe2zRzyvPD3SOYViWn7E1YvWOayMnHbDa7bGdYVd40XSAcKlgnu_wpddqHXIg5QYAe6bdA2LKOJJK-y0nwxQ7Y53FGQ2O9HgLjCBPRavqKfpkymqnvxGfMQIhREVBlYlxblpKM");

        Diverso produto4 = new Diverso(5, "Água");
        produto4.setPreco(new BigDecimal("2.50"));
        produto4.setImagem("https://www.shopsempre.com.br/media/catalog/product/cache/9d08971813a040f8f96067a40f75c615/a/g/agua-mineral-sem-gas-agua-da-pedra-garrafa-500ml.jpg");

        Diverso produto5 = new Diverso(5, "Pão de Queijo");
        produto5.setPreco(new BigDecimal("2.50"));
        produto5.setImagem("https://www.ibahia.com/fileadmin/user_upload/pao_de_queijo_2.jpg");

        Diverso produto6 = new Diverso(5, "Cookies");
        produto6.setPreco(new BigDecimal("2.00"));
        produto6.setImagem("https://img.cybercook.com.br/receitas/949/cookies-americanos-1.jpeg");

        Diverso produto7 = new Diverso(10, "Coca-cola lata");
        produto7.setPreco(new BigDecimal("5.00"));
        produto7.setImagem("https://cdn.garciasupermercados.com.br/media/catalog/product/cache/1/image/1000x1000/17f82f742ffe127f42dca9de82fb58b1/r/e/refrigerante_coca_cola_350ml.png");

        diversoRepository.salvar(produto1, produtoService.gerarCodigo());
        diversoRepository.salvar(produto2, produtoService.gerarCodigo());
        diversoRepository.salvar(produto3, produtoService.gerarCodigo());
        diversoRepository.salvar(produto4, produtoService.gerarCodigo());
        diversoRepository.salvar(produto5, produtoService.gerarCodigo());
        diversoRepository.salvar(produto6, produtoService.gerarCodigo());
        diversoRepository.salvar(produto7, produtoService.gerarCodigo());

    }
}
