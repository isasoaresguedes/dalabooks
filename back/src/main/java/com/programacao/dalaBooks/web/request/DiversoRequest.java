package com.programacao.dalaBooks.web.request;

public class DiversoRequest {

    int quantidade;
    String nome;
    String preco;
    String imagem;

    public String getImagem() {
        return imagem;
    }

    public int getQuantidade() {
        return quantidade;
    }

    public String getNome() {
        return nome;
    }

    public String getPreco() {
        return preco;
    }
}
