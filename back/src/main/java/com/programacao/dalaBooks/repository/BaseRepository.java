package com.programacao.dalaBooks.repository;

import java.util.List;

public interface BaseRepository<T> {

    public T salvar(T t);
    public T salvar(T t, int codigo);
    public List<T> listarTodos();
    //ver se n fica o mesmo do salvar sem o codigo
    public T atualizar(T t);
    public int size();
}
