package com.programacao.dalaBooks.repository;

import com.programacao.dalaBooks.domain.Registro;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class RegistroRepository implements BaseRepository<Registro> {

    private List<Registro> registros = new ArrayList<>();

    @Override
    public Registro salvar(Registro registro) {
        int codigo = this.size();
        registro.setCodigo(codigo);
        this.registros.add(registro);
        return registro;
    }

    @Override
    public Registro salvar(Registro registro, int codigo) {
        registro.setCodigo(codigo);
        this.registros.add(registro);
        return registro;
    }

    @Override
    public List<Registro> listarTodos() {
        return this.registros;
    }

    @Override
    public Registro atualizar(Registro registro) {
        return null;
    }

    @Override
    public int size() {
        return this.registros.size();
    }
}
