package com.programacao.dalaBooks.repository;

import com.programacao.dalaBooks.domain.Cliente;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class ClienteRepository implements BaseRepository<Cliente>{

    private List<Cliente> clientes = new ArrayList<>();

    @Override
    public int size() {
        return clientes.size();
    }

    @Override
    public Cliente atualizar(Cliente cliente) {
        this.clientes.set(cliente.getCodigo(), cliente);
        return cliente;
    }

    @Override
    public Cliente salvar(Cliente cliente) {
        int codigo = this.size();
        cliente.setCodigo(codigo);
        this.clientes.add(cliente);
        return cliente;
    }

    @Override
    public Cliente salvar(Cliente cliente, int codigo) {
        cliente.setCodigo(codigo);
        this.clientes.add(cliente);
        return cliente;
    }

    @Override
    public List<Cliente> listarTodos() {
        return this.clientes;
    }

    public Cliente findByCodigo(int codigo) {
        return this.clientes.get(codigo);
    }


}
