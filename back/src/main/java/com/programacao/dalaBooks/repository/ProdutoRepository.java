package com.programacao.dalaBooks.repository;

import java.util.List;

public interface ProdutoRepository<T> {

    public List<T> findIndisponiveis();
    public List<T> findDisponiveis();
    public void diminuirQuantidade(int codigo, int quantidade);
    public void aumentarQuantidade(int codigo, int quantidade);
}
