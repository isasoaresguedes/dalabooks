package com.programacao.dalaBooks.repository;

import com.programacao.dalaBooks.domain.Livro;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Repository
public class LivroRepository implements BaseRepository<Livro>, ProdutoRepository<Livro>{

    private List<Livro> livros = new ArrayList<>();

    @Override
    public Livro salvar(Livro livro) {
        this.livros.add(livro);
        return livro;
    }

    @Override
    public Livro salvar(final Livro livro, int codigo) {
        livro.setCodigo(codigo);
        this.livros.add(livro);
        return livro;
    }

    @Override
    public List<Livro> listarTodos() {
        return this.livros;
    }

    @Override
    public Livro atualizar(Livro livro) {
        for(int x=0; x<this.size(); x++) {
            if(livro == this.livros.get(x)) {
                this.livros.set(x, livro);
            }
        }
        return livro;
    }

    @Override
    public int size() {
        return livros.size();
    }

    @Override
    public List<Livro> findIndisponiveis() {
        return this.livros.stream().filter(livro -> livro.getQuantidade() == 0).collect(Collectors.toList());
    }

    @Override
    public void diminuirQuantidade(int codigo, int quantidade) {
        Livro livro = this.findByCodigo(codigo).get();
        livro.setQuantidade(livro.getQuantidade() - quantidade);
    }

    @Override
    public void aumentarQuantidade(int codigo, int quantidade) {
        Livro livro = this.findByCodigo(codigo).get();
        livro.setQuantidade(livro.getQuantidade() + quantidade);
    }

    @Override
    public List<Livro> findDisponiveis() {
        return this.livros.stream().filter(livro -> livro.getQuantidade() > 0).collect(Collectors.toList());
    }

    public Optional<Livro> findByCodigo(int codigo) {
        return this.livros.stream().filter(l -> l.getCodigo() == codigo).findFirst();
    }
}
