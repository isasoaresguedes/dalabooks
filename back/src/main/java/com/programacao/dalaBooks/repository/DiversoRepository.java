package com.programacao.dalaBooks.repository;

import com.programacao.dalaBooks.domain.Diverso;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Repository
public class DiversoRepository  implements BaseRepository<Diverso>, ProdutoRepository<Diverso>{

    private List<Diverso> diversos = new ArrayList<>();

    @Override
    public Diverso salvar(Diverso diverso) {
        this.diversos.add(diverso);
        return diverso;
    }

    @Override
    public Diverso salvar(final Diverso diverso, int codigo) {
        diverso.setCodigo(codigo);
        this.diversos.add(diverso);
        return diverso;
    }

    @Override
    public List<Diverso> listarTodos() {
        return this.diversos;
    }

    @Override
    public Diverso atualizar(Diverso diverso) {
        for(int x=0; x<this.size(); x++) {
            if(diverso == this.diversos.get(x)) {
                this.diversos.set(x, diverso);
            }
        }
        return diverso;
    }

    @Override
    public int size() {
        return diversos.size();
    }

    @Override
    public List<Diverso> findIndisponiveis() {
        return this.diversos.stream().filter(d -> d.getQuantidade() == 0).collect(Collectors.toList());
    }

    @Override
    public void diminuirQuantidade(int codigo, int quantidade) {
        Diverso diverso = this.findByCodigo(codigo).get();
        diverso.setQuantidade(diverso.getQuantidade() - quantidade);
    }

    @Override
    public void aumentarQuantidade(int codigo, int quantidade) {
        Diverso diverso = this.findByCodigo(codigo).get();
        diverso.setQuantidade(diverso.getQuantidade() + quantidade);
    }

    @Override
    public List<Diverso> findDisponiveis() {
        return this.diversos.stream().filter(d -> d.getQuantidade() > 0).collect(Collectors.toList());
    }

    public Optional<Diverso> findByCodigo(int codigo) {
        return this.diversos.stream().filter(d -> d.getCodigo() == codigo).findFirst();
    }
}
