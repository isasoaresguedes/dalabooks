package com.programacao.dalaBooks.service;

import com.programacao.dalaBooks.domain.Diverso;
import com.programacao.dalaBooks.domain.Livro;
import com.programacao.dalaBooks.exception.ValidacaoException;
import com.programacao.dalaBooks.repository.LivroRepository;
import com.programacao.dalaBooks.web.request.LivroRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Optional;

@Service
public class LivroService {

    @Autowired
    LivroRepository livroRepository;

    @Autowired
    ProdutoService produtoService;

    public Livro cadastrar(LivroRequest request) {
        Livro livro = new Livro(request.getQuantidade(), request.getNome(), request.getTipo());

        livro.setColecao(request.getColecao());
        livro.setPreco(new BigDecimal(request.getPreco()));
        livro.setAutor(request.getAutor());
        livro.setEditora(request.getEditora());
        livro.setGeneros(request.getGeneros());
        livro.setImagem(request.getImagem());

        return livroRepository.salvar(livro, produtoService.gerarCodigo());
    }

    public Livro atualizar(LivroRequest request, int codigo) {
        Optional<Livro> livroOptional = livroRepository.findByCodigo(codigo);

        if(!livroOptional.isPresent()) {
            throw new ValidacaoException("Produto não encontrado");
        }

        Livro livro = livroOptional.get();

        if(request.getAutor() != null) {
            livro.setAutor(request.getAutor());
        }
        if(request.getColecao() != null) {
            livro.setColecao(request.getColecao());
        }
        if(request.getEditora() != null) {
            livro.setEditora(request.getEditora());
        }
        if(request.getGeneros() != null) {
            request.getGeneros().forEach(genero -> livro.setGenero(genero));
        }
        if(request.getImagem() != null) {
            livro.setImagem(request.getImagem());
        }
        if(request.getNome() != null) {
            livro.setNome(request.getNome());
        }
        if(request.getPreco() != null) {
            livro.setPreco(new BigDecimal(request.getPreco()));
        }
        if(request.getQuantidade() > 0) {
            livro.setQuantidade(request.getQuantidade());
        }
        return livroRepository.atualizar(livro);
    }
}
