package com.programacao.dalaBooks.service;

import com.programacao.dalaBooks.domain.Cliente;
import com.programacao.dalaBooks.repository.ClienteRepository;
import com.programacao.dalaBooks.web.request.ClienteRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ClienteService {

    @Autowired
    ClienteRepository clienteRepository;

    public Cliente cadastrar(ClienteRequest request) {
        Cliente cliente = new Cliente();

        cliente.setLeitor(request.isLeitor());
        cliente.setNome(request.getNome());
        cliente.setPacote(request.getPacote());
        cliente.setFotoPerfil(request.getFoto());

        return clienteRepository.salvar(cliente);
    }

    public Cliente atualizar(ClienteRequest request, int codigo) {
        Cliente cliente = clienteRepository.findByCodigo(codigo);

        if(request.getPacote() != null) {
            cliente.setPacote(request.getPacote());
        }
        if(request.isLeitor() != null) {
            cliente.setLeitor(request.isLeitor());
        }
        if(request.getNome() != null) {
            cliente.setNome(request.getNome());
        }
        if(request.getFoto() != null) {
            cliente.setFotoPerfil(request.getFoto());
        }

        return clienteRepository.atualizar(cliente);
    }
}
