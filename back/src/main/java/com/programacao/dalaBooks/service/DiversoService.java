package com.programacao.dalaBooks.service;

import com.programacao.dalaBooks.domain.Diverso;
import com.programacao.dalaBooks.exception.ValidacaoException;
import com.programacao.dalaBooks.repository.DiversoRepository;
import com.programacao.dalaBooks.web.request.DiversoRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Optional;

@Service
public class DiversoService {

    @Autowired
    DiversoRepository diversoRepository;

    @Autowired
    ProdutoService produtoService;

    public Diverso cadastrar(DiversoRequest request) {
        Diverso diverso = new Diverso(request.getQuantidade(), request.getNome());

        diverso.setPreco(new BigDecimal(request.getPreco()));
        diverso.setImagem(request.getImagem());

        return diversoRepository.salvar(diverso, produtoService.gerarCodigo());
    }

    public Diverso atualizar(DiversoRequest request, int codigo) {
        Optional<Diverso> diversoOptional = diversoRepository.findByCodigo(codigo);

        if(!diversoOptional.isPresent()) {
            throw new ValidacaoException("Produto não encontrado");
        }

        Diverso diverso = diversoOptional.get();

        if(request.getNome() != null) {
            diverso.setNome(request.getNome());
        }
        if(request.getPreco() != null) {
            diverso.setPreco(new BigDecimal(request.getPreco()));
        }
        if(request.getQuantidade() > 0) {
            diverso.setQuantidade(request.getQuantidade());
        }
        if(request.getImagem() != null) {
            diverso.setImagem(request.getImagem());
        }

        return diversoRepository.atualizar(diverso);
    }
}
