package com.programacao.dalaBooks.service;

import com.programacao.dalaBooks.domain.*;
import com.programacao.dalaBooks.repository.ClienteRepository;
import com.programacao.dalaBooks.repository.DiversoRepository;
import com.programacao.dalaBooks.repository.LivroRepository;
import com.programacao.dalaBooks.repository.RegistroRepository;
import com.programacao.dalaBooks.web.request.VendaRequest;
import com.programacao.dalaBooks.web.response.VendaResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Service
public class VendaService {

    @Autowired
    DiversoRepository diversoRepository;

    @Autowired
    LivroRepository livroRepository;

    @Autowired
    ClienteRepository clienteRepository;

    @Autowired
    RegistroRepository registroRepository;

    public VendaResponse vender(VendaRequest request) {

        VendaResponse response = this.avaliarPedido(request);

        this.criarRegistro(request.getCodigoCliente(), response);

        return response;
    }

    private VendaResponse avaliarPedido(VendaRequest request) {
        VendaResponse response = new VendaResponse();
        List<Produto> produtosAprovados = new ArrayList<>();
        List<Integer> quantidadesAprovadas = new ArrayList<>();
        BigDecimal valorTotal = BigDecimal.ZERO;

        for(int x=0; x <request.getProdutos().size(); x++) {
            int codigo = request.getProdutos().get(x);
            Produto produto = this.procuraProduto(codigo);
            int quantidadeNecessaria = request.getQuantidades().get(x);

            if(this.pedidoProdutoValido(produto, quantidadeNecessaria)) {
                if (produto instanceof Livro) {
                    if (((Livro) produto).getTipo() == Tipo.VENDA) {
                        livroRepository.diminuirQuantidade(codigo, quantidadeNecessaria);
                        produtosAprovados.add(produto);
                        quantidadesAprovadas.add(request.getQuantidades().get(x));
                        valorTotal = this.calculaValorTotal(valorTotal, produto, quantidadeNecessaria);
                    }
                } else {
                    diversoRepository.diminuirQuantidade(codigo, quantidadeNecessaria);
                    produtosAprovados.add(produto);
                    quantidadesAprovadas.add(request.getQuantidades().get(x));
                    valorTotal = this.calculaValorTotal(valorTotal, produto, quantidadeNecessaria);
                }
            }
        };
        response.setProdutosProcessados(produtosAprovados);
        response.setQuantidadesProcessadas(quantidadesAprovadas);
        response.setValorTotal(valorTotal);
        return response;
    }

    private boolean pedidoProdutoValido(Produto produto, int quantidadeNecessaria) {
        return produto != null && produto.getQuantidade() >= quantidadeNecessaria;
    }

    private BigDecimal calculaValorTotal(BigDecimal valorTotal, Produto produto, int quantidadeNecessaria) {
        BigDecimal quantidadeVezesPreco = produto.getPreco().multiply(new BigDecimal(quantidadeNecessaria));
        return valorTotal.add(quantidadeVezesPreco);
    }

    private Produto procuraProduto(int codigo) {
        if(diversoRepository.findByCodigo(codigo).isPresent()) {
            return diversoRepository.findByCodigo(codigo).get();
        }
        else if(livroRepository.findByCodigo(codigo).isPresent()) {
            return livroRepository.findByCodigo(codigo).get();
        }
        return null;
    }

    private void criarRegistro(int codigoCliente, VendaResponse response) {
        Registro registro = new Registro();

        Cliente cliente = clienteRepository.findByCodigo(codigoCliente);
        registro.setCliente(cliente);
        registro.setValorFinal(response.getValorTotal());
        registro.setProdutos(response.getProdutosProcessados());
        registro.setQuantidadesAprovadas(response.getQuantidadesProcessadas());
        registro.setData(LocalDateTime.now());

        registroRepository.salvar(registro);
    }
}
