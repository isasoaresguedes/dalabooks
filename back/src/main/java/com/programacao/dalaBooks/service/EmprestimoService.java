package com.programacao.dalaBooks.service;

import com.programacao.dalaBooks.domain.*;
import com.programacao.dalaBooks.exception.ValidacaoException;
import com.programacao.dalaBooks.repository.ClienteRepository;
import com.programacao.dalaBooks.repository.LivroRepository;
import com.programacao.dalaBooks.repository.RegistroRepository;
import com.programacao.dalaBooks.web.request.EmprestimoRequest;
import com.programacao.dalaBooks.web.response.EmprestimoResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class EmprestimoService {

    @Autowired
    LivroRepository livroRepository;

    @Autowired
    ClienteRepository clienteRepository;

    @Autowired
    RegistroRepository registroRepository;

    public EmprestimoResponse emprestar(EmprestimoRequest request) {
        Cliente cliente = clienteRepository.findByCodigo(request.getCodigoCliente());

        if(cliente.getPacote() == Pacote.NENHUM) {
            throw new ValidacaoException("Cliente não possui pacote de empréstimo");
        }

        EmprestimoResponse response = this.avaliarLivros(request.getCodigos(), request.getQuantidades());
        BigDecimal valorTotal = this.calcularValor(response.getLivrosEmprestados(), cliente, response.getQuantidadesAprovadas());
        response.setValorTotal(valorTotal);

        this.criarRegistro(response, cliente);

        return response;
    }

    private BigDecimal calcularValor(List<Livro> livrosAprovados, Cliente cliente, List<Integer> quantidadesAprovadas) {
        int quantidadeTotal = livrosAprovados.size() * quantidadesAprovadas.size();
        return cliente.getPacote().valorPacote.multiply(new BigDecimal(quantidadeTotal));
    }

    private EmprestimoResponse avaliarLivros(List<Integer> codigos, List<Integer> quantidades) {
        EmprestimoResponse response = new EmprestimoResponse();
        List<Livro> livrosAprovados = new ArrayList<>();
        List<Integer> quantidadesAprovadas = new ArrayList<>();

        for(int x=0; x < codigos.size(); x++) {
            int codigo = codigos.get(x);
            Optional<Livro> livro = livroRepository.findByCodigo(codigo);
            if(livro.isPresent()) {
                int quantidadeNecessaria = quantidades.get(x);
                if(livro.get().getTipo() == Tipo.EMPRESTIMO && livro.get().getQuantidade() >= quantidadeNecessaria) {
                    livroRepository.diminuirQuantidade(codigo, quantidadeNecessaria);
                    livrosAprovados.add(livro.get());
                    quantidadesAprovadas.add(quantidades.get(x));
                }
            }
        }
        response.setQuantidadesAprovadas(quantidadesAprovadas);
        response.setLivrosEmprestados(livrosAprovados);
        response.setDataRetornar(this.formatarData());
        return response;
    }

    private void criarRegistro(EmprestimoResponse response, Cliente cliente) {
        Registro registro = new Registro();
        registro.setValorFinal(response.getValorTotal());
        registro.setCliente(cliente);
        registro.setLivrosEmprestados(response.getLivrosEmprestados());
        registro.setQuantidadesAprovadas(response.getQuantidadesAprovadas());
        registro.setData(LocalDateTime.now());
        registroRepository.salvar(registro);
    }

    private String formatarData() {
        DateTimeFormatter  dateFormat = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        return dateFormat.format(LocalDate.now().plusDays(7));
    }
}
