package com.programacao.dalaBooks.service;

import com.programacao.dalaBooks.domain.Diverso;
import com.programacao.dalaBooks.domain.Livro;
import com.programacao.dalaBooks.domain.Produto;
import com.programacao.dalaBooks.exception.ValidacaoException;
import com.programacao.dalaBooks.repository.DiversoRepository;
import com.programacao.dalaBooks.repository.LivroRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class ProdutoService {

    @Autowired
    DiversoRepository diversoRepository;

    @Autowired
    LivroRepository livroRepository;

    public int gerarCodigo() {
        return livroRepository.size() + diversoRepository.size();
    }

    public List<Produto> listarTodos() {
        List<Produto> lista = new ArrayList<>();
        lista.addAll(livroRepository.listarTodos());
        lista.addAll(diversoRepository.listarTodos());

        return lista;
    }

    public List<Produto> listarIndisponiveis() {
        List<Produto> lista = new ArrayList<>();
        lista.addAll(livroRepository.findIndisponiveis());
        lista.addAll(diversoRepository.findIndisponiveis());
        return lista;
    }

    public Produto solicitar(int codigo, int quantidade) {
        Optional<Livro> livro = livroRepository.findByCodigo(codigo);
        Optional<Diverso> diverso = diversoRepository.findByCodigo(codigo);

        if(livro.isPresent()) {
            this.livroRepository.aumentarQuantidade(codigo, quantidade);
            return livro.get();
        }
        else if(diverso.isPresent()) {
            this.diversoRepository.aumentarQuantidade(codigo, quantidade);
            return diverso.get();
        } else {
            throw new ValidacaoException("Produto não encontrado");
        }
    }

    public List<Produto> listarDisponiveis() {
        List<Produto> lista = new ArrayList<>();
        lista.addAll(livroRepository.findDisponiveis());
        lista.addAll(diversoRepository.findDisponiveis());
        return lista;
    }
}
