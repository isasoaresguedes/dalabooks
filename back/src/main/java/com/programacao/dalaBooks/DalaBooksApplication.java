package com.programacao.dalaBooks;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DalaBooksApplication {

	public static void main(String[] args) {
		SpringApplication.run(DalaBooksApplication.class, args);
	}

}
