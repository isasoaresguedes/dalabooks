package com.programacao.dalaBooks.domain;

public enum Genero {

    LITERATURA_FANTASTICA,
    ROMANCE,
    TERROR,
    ANJOS,
    HISTORIA,
    SOCIOLOGIA,
    FILOSOFIA
}
