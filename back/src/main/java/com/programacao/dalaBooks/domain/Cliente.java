package com.programacao.dalaBooks.domain;

import java.time.LocalDate;

public class Cliente {

    private int codigo;
    private String nome;
    private Boolean leitor;
    private LocalDate dataNascimento;
    private LocalDate dataCadastro;
    private Pacote pacote;
    private String fotoPerfil;

    public Cliente() {
    }

    public Cliente(String nome, boolean leitor, Pacote pacote) {
        this.nome = nome;
        this.leitor = leitor;
        this.pacote = pacote;
    }

    public Pacote getPacote() {
            return this.pacote;
        }

    public String getNome() {
        return nome;
    }

    public int getCodigo() {
        return codigo;
    }

    public boolean isLeitor() {
        return leitor;
    }

    public LocalDate getDataNascimento() {
        return dataNascimento;
    }

    public LocalDate getDataCadastro() {
        return dataCadastro;
    }

    public String getFotoPerfil() {
        return fotoPerfil;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public void setLeitor(boolean leitor) {
        this.leitor = leitor;
    }

    public void setDataNascimento(LocalDate dataNascimento) {
        this.dataNascimento = dataNascimento;
    }

    public void setDataCadastro(LocalDate dataCadastro) {
        this.dataCadastro = dataCadastro;
    }

    public void setPacote(Pacote pacote) {
        this.pacote = pacote;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public void setFotoPerfil(String fotoPerfil) {
        this.fotoPerfil = fotoPerfil;
    }
}
