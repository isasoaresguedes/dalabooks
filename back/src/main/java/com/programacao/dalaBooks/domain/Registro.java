package com.programacao.dalaBooks.domain;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

public class Registro {

    private int codigo;
    private Cliente cliente;
    private List<Produto> produtos;
    private List<Livro> livrosEmprestados;
    private List<Integer> quantidadesAprovadas;
    private BigDecimal valorFinal;
    private LocalDateTime data;
    private String dataEntrega;

    public Registro() {
    }

    public void setQuantidadesAprovadas(List<Integer> quantidadesAprovadas) {
        this.quantidadesAprovadas = quantidadesAprovadas;
    }

    public void setLivrosEmprestados(List<Livro> livrosEmprestados) {
        this.livrosEmprestados = livrosEmprestados;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public void setValorFinal(BigDecimal valorFinal) {
        this.valorFinal = valorFinal;
    }

    public void setProdutos(List<Produto> produtos) {
        this.produtos = produtos;
    }

    public void setData(LocalDateTime data) {
        this.data = data;
    }

    public void setDataEntrega(String dataEntrega) {
        this.dataEntrega = dataEntrega;
    }

    public List<Produto> getProdutos() {
        return produtos;
    }

    public BigDecimal getValorFinal() {
        return valorFinal;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public int getCodigo() {
        return codigo;
    }

    public List<Livro> getLivrosEmprestados() {
        return livrosEmprestados;
    }

    public List<Integer> getQuantidadesAprovadas() {
        return quantidadesAprovadas;
    }

    public LocalDateTime getData() {
        return data;
    }

    public String getDataEntrega() {
        return dataEntrega;
    }
}
