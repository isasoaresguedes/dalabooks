package com.programacao.dalaBooks.domain;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class Produto {

    private int codigo;
    private int quantidade;
    private String nome;
    private BigDecimal preco;
    private String imagem;

    public Produto(int quantidade, String nome) {
        this.quantidade = quantidade;
        this.nome = nome;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public void setQuantidade (int quantidade) {
        this.quantidade = quantidade;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public void setPreco(BigDecimal preco) {
        preco.setScale(2, RoundingMode.HALF_EVEN);
        this.preco = preco;
    }

    public void setImagem(String imagem) {
        this.imagem = imagem;
    }

    public int getCodigo() {
        return this.codigo;
    }

    public BigDecimal getPreco() {
        return this.preco;
    }

    public int getQuantidade() {
        return this.quantidade;
    }

    public String getNome() {
        return this.nome;
    }

    public String getImagem() {
        return imagem;
    }


}
