package com.programacao.dalaBooks.domain;

import java.util.ArrayList;
import java.util.List;

public class Livro extends Produto{

    private String autor;
    private String editora;
    private String colecao;
    private List<Genero> generos = new ArrayList<>();
    private Tipo tipo;

    public Livro(int quantidade, String nome, Tipo tipo) {
        super(quantidade, nome);
        this.tipo = tipo;
    }

    public void setAutor(String autor) {
        this.autor = autor;
    }

    public void setEditora(String editora) {
        this.editora = editora;
    }

    public void setColecao(String colecao) {
        this.colecao = colecao;
    }

    public void setGeneros(List<Genero> generos) {
        this.generos = generos;
    }

    public void setGenero(Genero genero) {
        this.generos.add(genero);
    }

    public void setTipo(Tipo tipo) {
        this.tipo = tipo;
    }

    public String getAutor() {
        return autor;
    }

    public String getEditora() {
        return editora;
    }

    public String getColecao() {
        return colecao;
    }

    public List<Genero> getGeneros() {
        return generos;
    }

    public Tipo getTipo() {
        return tipo;
    }

}
