package com.programacao.dalaBooks.domain;

import java.math.BigDecimal;

public enum Pacote {

    NENHUM(BigDecimal.ZERO),
    MENSAL(BigDecimal.ZERO),
    UNITARIO(new BigDecimal("2.50"));

    public BigDecimal valorPacote;
    Pacote(BigDecimal valor) {
        valorPacote = valor;
    }
}
